from netgen.csg import *
from ngsolve.internal import visoptions
from netgen.meshing import *
from ngsolve import *

from sphere_problem_datafcts import force, exu, dexu
from math import pi

from HDG import SolveHDG
from HdivHDG import SolveHdivHDG
from H1_Lag import SolveH1Lagrange
from H1_Pen import SolveH1Penalty
from PseudoDG import SolvePseudoDG
from PseudoHdivDG import SolvePseudoHdivDG

geo = CSGeometry()

r = 1
sphere = Sphere(Pnt(0,0,0),r).bc("dir")
geo.Add(sphere)
mesh = Mesh(geo.GenerateMesh(perfstepsend=MeshingStep.MESHSURFACE,optsteps2d=3,maxh=20))

Draw(mesh)

normal = specialcf.normal(3)
nmat = CoefficientFunction(normal, dims =(3,1))
proj = Id(3) - nmat*nmat.trans

Draw(cf = proj*exu, mesh = mesh, name = "ex_u")
Draw(cf =force, mesh = mesh, name = "force")

quantities = ["ndof", "ncdof", "nze", "L2Error_tang", "L2Error_normal", "H1Error"]
methods = ["PseudoDG", "PseudoHdivDG", "HDG", "HdivHDG", "H1Lag", "H1Pen"]

nref = 5

eval_quantities = dict()
for method in methods:
    eval_quantities[method] = dict()
    for error in quantities:
        eval_quantities[method][error] = [ None for i in range(nref)]

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--ku", help="order", type=int)
parser.add_argument("--kg", help="geom_order", type=int)
parser.add_argument("-le","--lifterror", dest='lifterror', action='store_true', help='lift the error on a (by one order) higher accurate surface')
args = parser.parse_args()
# kwargs = dict(order

if args.ku == None:
    args.ku = 1
    print("Setting ku to", args.ku)

if args.kg == None:
    args.kg = args.ku + 1
    print("Setting kg to", args.kg)

condense = True
order = args.ku
geomorder = args.kg
lifterror = args.lifterror

print(eval_quantities)
for ref in range(nref):
    if ref != 0:
        mesh.Refine(mark_surface_elements=True)
        
    mesh.Curve(geomorder)
    
    h = specialcf.mesh_size
    
    Draw(mesh)
    if "HDG" in methods:
        local_errs = SolveHDG( mesh, exu = exu, ex_du = dexu, force = force, order = order, condense=condense, alpha = 10, label="HDG", lifterror=lifterror)
        mesh.Curve(geomorder)
        for i, error in enumerate(quantities):
            eval_quantities["HDG"][error][ref] = local_errs[i]
    if "HdivHDG" in methods:
        local_errs = SolveHdivHDG( mesh, exu = exu, ex_du = dexu, force = force, order = order, condense=condense, alpha = 10, label="HdivDG", lifterror=lifterror)
        mesh.Curve(geomorder)
        for i, error in enumerate(quantities):
            eval_quantities["HdivHDG"][error][ref] = local_errs[i]
    if "H1Lag" in methods:
        local_errs = SolveH1Lagrange( mesh, exu = exu, ex_du = dexu, force = force, order = order, condense=condense, label="H1Lag", lifterror=lifterror)
        mesh.Curve(geomorder)
        for i, error in enumerate(quantities):
            eval_quantities["H1Lag"][error][ref] = local_errs[i]
    if "H1Pen" in methods:
        local_errs = SolveH1Penalty( mesh, exu = exu, ex_du = dexu, force = force, order = order, condense=condense,
                                     penalty = 10*h**(-order-1),
                                     # penalty = h**(-2),
                                     label="H1Pen", lifterror=lifterror)
        mesh.Curve(geomorder)
        for i, error in enumerate(quantities):
            eval_quantities["H1Pen"][error][ref] = local_errs[i]

    if "PseudoDG" in methods:
        local_errs = SolvePseudoDG( mesh, order = order, label="PseudoDG", lifterror=lifterror)
        mesh.Curve(geomorder)
        for i, error in enumerate(quantities):
            eval_quantities["PseudoDG"][error][ref] = local_errs[i]

    if "PseudoHdivDG" in methods:
        local_errs = SolvePseudoHdivDG( mesh, order = order, label="PseudoHdivDG", lifterror=lifterror)
        mesh.Curve(geomorder)
        for i, error in enumerate(quantities):
            eval_quantities["PseudoHdivDG"][error][ref] = local_errs[i]

    print(eval_quantities)    



def write(string):
    outfile.write(string)
    print(string,end="")
        
for quantity in quantities:
    outfile = open("data_sphere/comparison_"+quantity+"_Pk"+str(order)+"_Pg"+str(geomorder)+".dat","w")
    write("#"+quantity+"\n")
    write("#ref"+"\t")
    for method in methods:
        write(method+"\t")
    write("\n")
    for ref in range(nref):
        write(str(ref)+"\t")
        for method in methods:
            write(str(eval_quantities[method][quantity][ref])+"\t")
        write("\n")
    write("\n")

    outfile.close()    
    
